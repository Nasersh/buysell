// To parse this JSON data, do
//
//     final product = productFromJson(jsonString);

import 'dart:convert';

Products productsFromJson(String str) =>  Products.fromJson(json.decode(str));

List<Product> productFromJson(String str) =>
    List<Product>.from(json.decode(str).map((x) => Product.fromJson(x)));

String productsToJson(Product data) => json.encode(data.toJson());

class Products {
  Products({
    required this.products,
  });

  List<Product> products;

  factory Products.fromJson(Map<String, dynamic> json) => Products(
    products: List<Product>.from(json["products"].map((x) => Product.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "products": List<dynamic>.from(products.map((x) => x.toJson())),
  };
}

class Product   {

  Product({
     this.id=0,
    required this.name,
   this.imageUrl='',
    required this.expDate,
    required this.categoryId,
     this.userId=0,
    required this.quantity,
    required this.price,
     this.current_price=0,
  });
  var id;
  var name;
  var imageUrl;
  var expDate;
  var categoryId;
  var userId;
  var quantity;
  var price;
  var current_price;


  factory Product.fromJson(Map<String, dynamic> json) => Product(
    id: json["id"],
    name: json["name"],
    imageUrl: json["image_url"],
    expDate: DateTime.parse(json["exp_date"]),
    categoryId: json["category_id"],
    userId: json["user_id"],
    quantity: json["quantity"],
    price: json["price"], current_price: json["current_price"],

  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "image_url": imageUrl,
    "exp_date": "${expDate.year.toString().padLeft(4, '0')}-${expDate.month.toString().padLeft(2, '0')}-${expDate.day.toString().padLeft(2, '0')}",
    "category_id": categoryId,
    "user_id": userId,
    "quantity": quantity,
    "price": price,
  };
}
