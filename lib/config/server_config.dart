class ServerConfig{

  static const domainNameServer = 'http://192.168.5.82:8000';

 static const register = '/api/user/register';

  static const check_valid = '/api/auth/check_validity';

 static const Signin= '/api/user/login';

 static const Logout= '/api/user/logout';

 static const getCategories= '/api/categories/';

 static const showallProducts= '/api/products';

 static const showUserProducts= '/api/user/products';

 static const showUserDetails= '/api/user/show/';

 static const deleteproduct= '/api/products/delete/';

 static const showCategoryProducts= '/api/products/search/category/';

 static const SearchProducts= '/api/products/search/name/';

 static const DetailsProduct= '/api/products/show/';

  static const AddProduct='/api/products/';

static const updateProduct='/api/products/update/';

}