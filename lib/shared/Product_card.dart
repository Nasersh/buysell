
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shop1/models/product.dart';
import 'package:shop1/modules/in_product/in_product.dart';

import '../constants.dart';

Widget Buildcard(String name,int price,String imgpath,bool isfav,Product Productmodel)
{
  return  InkWell(
    onTap: (){
      Get.to(() => InProduct(productmodel: Productmodel,));
    },
    child: Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        boxShadow: [
          kDefaultShadow
        ],
        color: Colors.white,
      ),
      child: Column(
        children: [
        Container(
         width: 100,
         height: 70,
         decoration: BoxDecoration(
           // image: DecorationImage(
           //   image: NetworkImage(
           //      ''),
           //   fit: BoxFit.fill,
           // ),
         ),
        ),
        SizedBox(width: 8),
        Expanded(
         child: Padding(
           padding: const EdgeInsets.all(8.0),
           child: Column(
             crossAxisAlignment: CrossAxisAlignment.start,
             children: [
               Text(
                 name,
                 style: TextStyle(
                   color: kPrimaryColor,
                   fontWeight: FontWeight.bold,
                 ),
                 maxLines: 1,
                 overflow: TextOverflow.ellipsis,
               ),
               Expanded(
                 child: Text(
                   'buy that product please',
                   maxLines: 3,
                   overflow: TextOverflow.ellipsis,
                 ),
               ),
               Text(
                 "\$${price}",
                 style: TextStyle(
                   fontWeight: FontWeight.bold,
                 ),
               ),
             ],
           ),
         ),
        )
        ],
    ),
      ),
  );
}
