
import 'package:flutter/cupertino.dart';

import '../constants.dart';

class CustomButton extends StatelessWidget {
  final double width;
  final double height;
  final String buttonName;
  final Function() onTap;
  final Color buttonColor;
  final Color ? fontColor;

  CustomButton({
    required this.height,
    required this.width,
    required this.buttonName,
    required this.onTap,
    required this.buttonColor,
    this.fontColor,
  });
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        width: width,
        height: height ,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(50),
            color: buttonColor
        ),
        child: Center(
          child:  Text(
            '$buttonName',
            style:  TextStyle(
              fontSize: 20,
              color: fontColor ?? firstBackgroundColor ,
            ),
          ),
        ),
      ),
    );
  }
}
