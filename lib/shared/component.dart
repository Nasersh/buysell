import 'package:flutter/material.dart';

import '../constants.dart';


Widget TextAdd
    ({ required final String htext,
  required final Widget ic,
  required final Function (String) onchange,
  final Color c=kBackgroundColor,

})

{
  TextEditingController addcontroller=new TextEditingController();
  return Padding(
      padding:  EdgeInsets.symmetric(horizontal: 20),
      child: Container(
        height:60,
        margin:  EdgeInsets.symmetric(horizontal: 10,vertical: 5) ,
        decoration:   BoxDecoration(
          color:kSelectedColor,
          borderRadius: BorderRadius.all(Radius.circular(20)),
        ),
        child:  Center(
          child: Padding(
            padding:  EdgeInsets.symmetric(horizontal: 10),
            child: TextField(
              controller: addcontroller,
              onChanged: onchange,
              style: TextStyle(color:Colors.white),
              decoration: InputDecoration(
                border: InputBorder.none,
                // focusedBorder: InputBorder.none,
                prefixIcon: ic,
                hintText: htext,
                hintStyle: const TextStyle(
                  color: Colors.white,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }



Widget SignInText
({ required final String htext,
  required final Widget ic,
  required final Function (String) onchange,
  final Color c=kBackgroundColor,
})
{

  return   Container(
    height: 55,
    width: 320,
    margin: const EdgeInsets.all(8.0),
    decoration: BoxDecoration(
        color: c, borderRadius: BorderRadius.circular(30)),
    child:  Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10.0),
      child: TextField(
        onChanged: onchange,
        style: const TextStyle(
          color: Colors.black,
        ),
        cursorColor:kPrimaryColor,
        decoration: InputDecoration(
            enabledBorder: const OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(20)),
                borderSide: BorderSide(color: kPrimaryColor)),
            focusedBorder:  const OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(20)),
                borderSide: BorderSide(color: kPrimaryColor)),
            icon: ic,
            labelText: htext,
            labelStyle: const TextStyle(color: kPrimaryColor)),
      ),
    ),
  );}


Widget SignUpText
    ({ required final String htext,
  required final Widget ic,
  required final Function (String) onchange,
})
{
  return Padding(
    padding: const EdgeInsets.symmetric(horizontal: 20),
    child: Container(
      height: 60,
      margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(20)),
      ),
      child:  Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15),
        child: TextField(
          onChanged: onchange,
          decoration: InputDecoration(
              prefixIcon: ic,
              hintText: htext,
              hintStyle: const TextStyle(
                color: Colors.deepPurple,
              )),
        ),
      ),
    ),
  );
}

