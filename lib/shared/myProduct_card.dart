import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shop1/models/product.dart';
import 'package:shop1/modules/in_product/in_product.dart';

import '../constants.dart';

Widget BuildMycard(String name,int price,String imgpath,bool isfav,Product Productmodel,Function func )
{
  return  InkWell(
    onTap: (){
      Get.to(() => InProduct(productmodel: Productmodel,));
    },
    child: Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        boxShadow: [
          kDefaultShadow
        ],
        color: Colors.white,
      ),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              IconButton(
                icon: Icon(Icons.delete,color: kPrimaryColor,), onPressed: () {func();},
              ),
            ],
          ),
        Container(
         width: 100,
         height: 60,
         decoration: BoxDecoration(

         ),
        ),
        SizedBox(height: 8),
        Expanded(
         child: Padding(
           padding: const EdgeInsets.all(8.0),
           child: Row(
             crossAxisAlignment: CrossAxisAlignment.start,
             children: [
               Text(
                 name,
                 style: TextStyle(
                   color: kPrimaryColor,
                   fontWeight: FontWeight.bold,
                 ),
                 maxLines: 1,
                 overflow: TextOverflow.ellipsis,
               ),
               Text(
                 "\$${price}",
                 style: TextStyle(
                   fontWeight: FontWeight.bold,
                 ),
               ),
             ],
           ),
         ),
        )
        ],
    ),
      ),
  );
}
