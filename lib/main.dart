import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';
import 'package:shop1/modules/additions/add_new.dart';
import 'constants.dart';
import 'modules/in_app/app.dart';
import 'modules/log_in/log_in.dart';
import 'modules/products/prod.dart';
import 'modules/register/register.dart';

void main() {
  runApp(
      GetMaterialApp(
        theme: ThemeData(primaryIconTheme: IconThemeData(color: kPrimaryColor)),
        debugShowCheckedModeBanner: false,
        initialRoute: '/login',
        getPages: [
          GetPage(name: '/login', page: ()=> LogIn()),
          GetPage(name: '/register', page: ()=>Register()),
          GetPage(name: '/products', page: ()=>Prod()),
          GetPage(name: '/inapp', page: ()=>InApp()),
          GetPage(name: '/addpro', page: ()=>AddNew()),
        ],
        builder: EasyLoading.init(),
  )
  );
}
