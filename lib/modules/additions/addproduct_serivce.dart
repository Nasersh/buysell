
import 'package:http/http.dart'as http;
import 'package:shop1/config/server_config.dart';
import 'package:shop1/models/product.dart';
import 'package:shop1/native_service/secure_storage.dart';
class addProductService{
  var urlAddProduct=Uri.parse(ServerConfig.domainNameServer+ServerConfig.AddProduct);


  // to add product
    sendDataToAddProduct(Product product )async{
      var _storage = SecureStorage();
      print('');
    String? token = await _storage.read('token');
    var response=await http.post(urlAddProduct,
        headers:{
          'Accept' : 'application/json',
          'token' : 'Bearer $token'
        },
        body: {

      "name":product.name ,
      "category_id": product.categoryId,
      "user_id": product.userId,
      "quantity": product.quantity,
      "price": product.price,
      "exp_date":product.expDate ,

    });
    print(response.statusCode);
    print(response.body);



  }




}