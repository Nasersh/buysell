

import 'package:get/get.dart';
import 'package:shop1/models/categories_model.dart';
import 'package:shop1/models/product.dart';
import 'package:shop1/modules/categories/categories_controller.dart';

import 'addproduct_serivce.dart';

class addProductController extends GetxController {
  var categoryId ;
  var name ='remon';
  var expDate ;
  var message;
  var imageUrl;
  var quantity;
  var price;
  var currentprice;
  var addingstatus=false;


  addProductService service = addProductService();

  Future<void> AddProductOnClick()async {
    Product product = Product(
      name: name,
      expDate: expDate,
      categoryId: int.parse(categoryId),
      quantity: int.parse(quantity),
      price:int.parse(price),
    );
    addingstatus=await service.sendDataToAddProduct(product);
  }


}