import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:shop1/constants.dart';
import 'package:shop1/shared/component.dart';
import 'addproducts_controller.dart';

class AddNew extends StatefulWidget {
  AddNew({Key? key}) : super(key: key);

  @override
  State<AddNew> createState() => _AddNewState();
}

class _AddNewState extends State<AddNew> {

  TextEditingController dateController = TextEditingController();

  addProductController addcontroller=Get.put(addProductController());

  static const menuItems = <String>[
    '1',
    '2',
    '3',
  ];
  final List<DropdownMenuItem<String>> _dropDownMenuItems = menuItems
      .map(
        (String value) => DropdownMenuItem<String>(
      value: value,
      child: Text(value),
    ),
  )
      .toList();
  final List<PopupMenuItem<String>> _popUpMenuItems = menuItems
      .map(
        (String value) => PopupMenuItem<String>(
      value: value,
      child: Text(value),
    ),
  )
      .toList();
  String _btn1SelectedVal = 'category1';

  @override
  Widget build(BuildContext context) {
    TextEditingController addprice = TextEditingController();
    return Scaffold(
      body: SingleChildScrollView(
        child: Stack(
          children: [
            Container(
              padding: EdgeInsets.all(15),
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(kPrimaryColor),
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                          side: const BorderSide(
                            color: Colors.white,
                            width: 2.0,
                          ),
                        ),
                      ),
                    ),
                    child: const Text(
                      'Submit',
                      style: TextStyle(color: Colors.white),
                    ),
                    onPressed: () {
                     addcontroller.AddProductOnClick();
                    },
                  ),
                ],
              ),
            ),
            Container(
              child: Column(
                children: [
                  SizedBox(height: 100),
                  Text(
                    'Fill the product Card',
                    style: GoogleFonts.oswald(
                        textStyle:
                        const TextStyle(color: Colors.white, fontSize: 35)),
                  ),
                  SizedBox(height: 40),
                  Container(
                    height: MediaQuery.of(context).size.height * .6,
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          TextAdd(
                            htext: 'name',
                            ic: const Icon(
                              Icons.person,
                              color: Colors.white,
                            ),
                            onchange: (value) {addcontroller.name=value;},
                          ),
                          const SizedBox(
                            height: 16,
                          ),
                          Padding(
                            padding:  EdgeInsets.symmetric(horizontal: 20),
                            child: Container(
                              height:60,
                              margin:  EdgeInsets.symmetric(horizontal: 10,vertical: 5) ,
                              decoration:   BoxDecoration(
                                color:kSelectedColor,
                                borderRadius: BorderRadius.all(Radius.circular(20)),
                              ),
                              child:   Center(
                                child: Padding(
                                  padding:  EdgeInsets.symmetric(horizontal: 10),
                                  child: TextField(
                                    controller: addprice,
                                    onChanged: (value)
                                    {

                                    },
                                    style: const TextStyle(color:Colors.white),
                                    decoration: const InputDecoration(
                                      border: InputBorder.none,
                                      // focusedBorder: InputBorder.none,
                                   //   prefixIcon: ic,
                                    //  hintText: htext,
                                      hintStyle: TextStyle(
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 16,
                          ),
                          TextAdd(
                            htext: 'quantity',
                            ic: const Icon(
                              Icons.person_outlined,
                              color: Colors.white,
                            ),
                            onchange: (value) {
                              int s=int.parse(value);
                              addcontroller.quantity=s;},
                          ),
                          const SizedBox(
                            height: 16,
                          ),
                          TextAdd(
                            htext: 'cat id',
                            ic: const Icon(
                              Icons.person_outlined,
                              color: Colors.white,
                            ),
                            onchange: (value) {
                              int s=int.parse(value);
                              addcontroller.categoryId=s;},
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          // Padding(
                          //   padding: const EdgeInsets.symmetric(horizontal: 27.0),
                          //   child: Container(
                          //     padding: EdgeInsets.symmetric(horizontal: 20),
                          //     decoration: BoxDecoration(color: kSelectedColor,borderRadius: BorderRadius.circular(20
                          //     )
                          //     ),
                          //     child: TextFormField(
                          //       controller: dateController,
                          //       decoration:  InputDecoration(
                          //         border: InputBorder.none,
                          //         labelStyle: TextStyle(color: Colors.white),
                          //         labelText: '  Expiration Date ',
                          //         suffixIcon:  Icon(Icons.calendar_today,color: kSelectedColor,),
                          //       ),
                          //       readOnly: true,
                          //       validator: (value) {
                          //         if (value!.isEmpty) {
                          //           return 'Enter Expiration Date';
                          //         }
                          //         return null;
                          //       },
                          //       onTap: () async {
                          //         DateTime? pickedDate = await showDatePicker(
                          //             context: context,
                          //             initialDate: DateTime.now(),
                          //             firstDate: DateTime(2020),
                          //             lastDate: DateTime(2101));
                          //
                          //         if (pickedDate != null) {
                          //           print(pickedDate);
                          //           String formattedDate =
                          //           DateFormat('yyyy-MM-dd').format(pickedDate);
                          //           print(formattedDate);
                          //           setState(() {
                          //             dateController.text = formattedDate;
                          //             addcontroller.expDate=formattedDate;
                          //           });
                          //         }
                          //       },
                          //     ),
                          //   ),
                          // ),
                          // const SizedBox(
                          //   height: 16,
                          // ),

                          // Padding(
                          //   padding: const EdgeInsets.symmetric(horizontal: 20),
                          //   child: ListTile(
                          //     title: const Text(
                          //       'Category :',
                          //       style: TextStyle(
                          //         color: Colors.white,
                          //       ),
                          //     ),
                          //     trailing: DropdownButton<String>(
                          //       style: TextStyle(color: Colors.grey),
                          //       value: _btn1SelectedVal,
                          //       onChanged: (String? newValue) {
                          //         if (newValue != null) {
                          //           setState(() {
                          //             _btn1SelectedVal = newValue;
                          //             addcontroller.categoryId=newValue;
                          //           } );
                          //         }
                          //       },
                          //       items: _dropDownMenuItems,
                          //     ),
                          //   ),
                          // ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              height: MediaQuery.of(context).size.height - 80,
              decoration: const BoxDecoration(
                  color: kPrimaryColor,
                  borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(40),
                      bottomLeft: Radius.circular(40))),
            ),
          ],
        ),
      ),
    );
  }



}
