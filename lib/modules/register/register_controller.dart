import 'package:get/get.dart';
import 'package:shop1/models/user.dart';
import 'package:shop1/modules/register/register_service.dart';

class RegisterController extends GetxController{

  var fullName = '';
  var email = '';
  var password ='';
  var passwordConfirm = '';
  var signupstatus=false;
  var message='';
  var phone='';

  SignUpService service = SignUpService();

  Future<void> registerOnClick()async{
        UserClass user = UserClass(
          name: fullName,
          email: email,
          password: password,
          passwordConfirmation: passwordConfirm,
          phone: phone,
        );
        signupstatus=await service.register(user);
  }



}