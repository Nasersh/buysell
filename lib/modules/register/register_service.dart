import 'dart:convert';

import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:shop1/config/server_config.dart';
import 'package:shop1/models/user.dart';
import 'package:shop1/modules/in_app/app.dart';

class SignUpService {

  var url = Uri.parse(ServerConfig.domainNameServer + ServerConfig.register);
  var message;
  register(UserClass user) async{
    var response = await http.post(
      url,
      headers: {
        'Accept' : 'application/json',
      },
      body: {
        'name': user.name,
        'email': user.email,
        'password':user.password,
        'c_password':user.passwordConfirmation,
        'phone':user.phone,
      }
    );

    print(response.statusCode);
    print(response.body);
    if(response.statusCode==200)
      {
        var jsonresponse =jsonDecode(response.body);
        message=jsonresponse['message'];
        Get.off(InApp());
        return true;
      }
    else if(response.statusCode==422)
    {
      var jsonresponse =jsonDecode(response.body);
      message=jsonresponse['message'];
      return false;
    }
    else
      {
        print('error');
        return false;

      }
  }
}