import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shop1/constants.dart';
import 'package:shop1/modules/register/register_controller.dart';
import 'package:shop1/shared/component.dart';

class Register extends StatefulWidget {


  Register({Key? key}) : super(key: key);

  @override
  State<Register> createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  RegisterController controller=Get.put(RegisterController());

  File? image;
  final picker = ImagePicker();
  Future pickImage() async {
    final Image = await ImagePicker().pickImage(source: ImageSource.gallery);
    if (image == null) return;
    final imageT = File(Image!.path);

    setState(() => this.image = imageT);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: kPrimaryColor,
        appBar: AppBar(
          backgroundColor: kPrimaryColor,
          elevation: 0.0,
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: const Icon(
              Icons.arrow_back,
            ),
          ),
        ),
        body: SingleChildScrollView(
          child: Column(children: [
            Text(
              'Sign up!',
              style: GoogleFonts.pacifico(textStyle: const TextStyle(
                fontSize: 30,
                fontWeight: FontWeight.w600,
                color: Colors.white
              )),
            ),
            const SizedBox(
              height: 35,
            ),
            TextAdd(htext: 'Enter your name',
              ic:   const Icon(
                Icons.person_outline
              , color: Colors.white), onchange: (value)
                {
                  controller.fullName=value;
                },),
            const SizedBox(height: 40,),
            TextAdd(htext: 'Enter your Email',
                ic: const Icon(
                  Icons.alternate_email_outlined,
                  color: Colors.white,
                ), onchange: (value)
              {
                controller.email=value;
              },
       ),
            const SizedBox(
              height: 40,
            ),
            TextAdd(
              htext: 'Password',
              ic: const Icon(
                Icons.vpn_key_outlined,
                color: Colors.white,
              ), onchange: (value){
                controller.password=value;
            },
            ),
            const SizedBox(
              height: 40,
            ),
            TextAdd(
              htext: 'Password Confirmation',
              ic: const Icon(
                Icons.vpn_key_outlined,
                color: Colors.white,
              ), onchange: (value){
                controller.passwordConfirm=value;
            },
            ),
            const SizedBox(
              height: 40,
            ),
            TextAdd(
              htext: 'Phone',
              ic: const Icon(
                Icons.vpn_key_outlined,
                color: Colors.white,
              ),
              onchange: (value){
              controller.phone=value;
            },
            ),
            ElevatedButton(
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(kPrimaryColor),
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(18.0),
                    side: const BorderSide(
                      color: Colors.white,
                      width: 2.0,
                    ),
                  ),
                ),
              ),
              child: const Text(
                'Submit',
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () {
                pickImage();
              },
            ),
            const SizedBox(
              height: 30,
            ),
            const SizedBox(
              height: 40,
            ),
            ElevatedButton(
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(kPrimaryColor),
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(18.0),
                    side: const BorderSide(
                      color: Colors.white,
                      width: 2.0,
                    ),
                  ),
                ),
              ),
              child: const Text(
                'Submit',
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () {
                RigesterOnClick();
              },
            ),
            const SizedBox(
              height: 30,
            ),
            Container(
                width: 100,
                decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                ),
                child: FlatButton(
                  onPressed: () {},
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const [
                      Icon(
                        Icons.facebook,
                        color: kPrimaryColor,
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Icon(
                        Icons.call,
                        color: kPrimaryColor,
                      )
                    ],
                  ),
                )),
            const SizedBox(
              height: 40,
            ),
          ]),
        ));
  }

   void RigesterOnClick ()
   async{
     EasyLoading.show(status: 'Loading ...');
     await controller.registerOnClick();
     if (controller.signupstatus) {
       EasyLoading.showSuccess(controller.message);
     } else {
       EasyLoading.showError(
         controller.message,
         duration: Duration(seconds: 10),
         dismissOnTap: true,
       );
       print('error here');
     }
   }
}


