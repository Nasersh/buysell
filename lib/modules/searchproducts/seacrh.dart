import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shop1/modules/searchproducts/searchcontroller.dart';
import 'package:shop1/shared/Product_card.dart';
import 'package:shop1/shared/search_box.dart';
import '../../constants.dart';

class ssearch extends StatelessWidget {
  ssearch({Key? key}) : super(key: key);
  String name = '';
  SearchController scontroller = Get.put(SearchController());

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [

        Flex(

          direction: Axis.vertical,
          children:[Expanded(
            child: Column(
                children: [
                  Container(
                    height: 250,
                    decoration: const BoxDecoration(
                        color: kPrimaryColor,
                        borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(40),
                            bottomRight: Radius.circular(40))),
                    child: Column(
                      children: [
                        const SizedBox(
                          height: 45,
                        ),
                        SearchBox(
                          onChanged: (value) {
                            name = value;
                          },
                        ),
                        const SizedBox(
                          height: 1,
                        ),
                        ElevatedButton(
                          style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all(kPrimaryColor),
                            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                              RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(18.0),
                                side: const BorderSide(
                                  color: Colors.white,
                                  width: 2.0,
                                ),
                              ),
                            ),
                          ),
                          child: const Text(
                            'SEARCH',
                            style: TextStyle(color: Colors.white),
                          ),
                          onPressed: () {
                            scontroller.Searchname(name);
                          },
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 35),
                          child: Text(
                            'Search For Products',
                            style: GoogleFonts.oswald(
                              textStyle: const TextStyle(
                                  color: Colors.white,
                                  fontSize: 25,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                      ],
                    ),
                  ),
                  Column(
                      children: [

                        Container(
                          // margin: EdgeInsets.only(top: 0),
                          decoration: const BoxDecoration(
                            color: kBackgroundColor,
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(40),
                              topRight: Radius.circular(40),
                            ),
                          ),
                        ),
                        Obx(() {
                          if (scontroller.isLoading.value) {
                            return  Column(
                              children: [
                                Container(
                                  // height:300,
                                  // width:MediaQuery.of(context).size.width,
                                  color: kBackgroundColor,),
                              ],
                            );
                          }
                          else {
                            return _ProductsList();
                          }})
                      ]),
                ]),
          ),]
        ),
      ],
    );
  }
  Widget _ProductsList()
  {
    return Container(
      height: 300,
      child: ListView(primary: false, shrinkWrap: true,children: [
        const SizedBox(
          height: 30,
        ),
        GridView.count(
          physics: ClampingScrollPhysics(),
          padding: EdgeInsets.fromLTRB(8, 0, 8, 0),
          shrinkWrap: true,
          crossAxisCount: 2,
          mainAxisSpacing: 30,
          crossAxisSpacing: 20,
          children:
          List.generate(scontroller.SearchList.length, (index) {
            return Buildcard(
                scontroller.SearchList[index].name,
                scontroller.SearchList[index].id,
                scontroller.SearchList[index].imageUrl,
                false,scontroller.SearchList[index] );
          }),
        )
      ]),
    );
  }
}
