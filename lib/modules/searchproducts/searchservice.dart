import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:shop1/config/server_config.dart';
import 'package:shop1/models/product.dart';


class SearchService{

  var message;
  var url = Uri.parse(ServerConfig.domainNameServer + ServerConfig.SearchProducts);

  Future<List<Product>> getCategories(
      //String token
      ) async{
    var response = await http.get(url);
    print(response.statusCode);
   // print(response.body);

    if(response.statusCode == 200){
      var SearchProducts = productsFromJson(response.body);
      print(SearchProducts.products);
      return SearchProducts.products;
    }
    else {
      return [];
    }
  }
}