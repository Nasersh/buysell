

import 'package:get/get.dart';
import 'package:shop1/models/product.dart';
import 'package:shop1/modules/products/products_controller.dart';
import 'package:shop1/modules/searchproducts/searchservice.dart';

class   SearchController extends GetxController{

  List SearchList = <Product>[].obs;
 // SearchService _service = SearchService();
  ProductsController productsController=ProductsController();
  var isLoading  = true.obs;

  var searchbyname;


  @override
  void onInit() {
    //searchbyname='';
    super.onInit();
  }

  Searchname(name) async {
    SearchList=await productsController.searchProductsFromRepo(name);
    isLoading.value=false;
  }
}