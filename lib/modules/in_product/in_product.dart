import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:shop1/models/product.dart';
import '../../constants.dart';
import 'inprodutcontroller.dart';

class InProduct extends StatelessWidget {
  InProductController controller=InProductController();

   InProduct( {Key? key, required this.productmodel}) : super(key: key);
   final Product productmodel;

  @override
  Widget build(BuildContext context) {
    print(productmodel.name);
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        color: Colors.white,
        child:
    //     Obx(()
    // {
      //   if (controller.isLoading.isTrue) {
      //     return const Center(
      //       child: CircularProgressIndicator(
      //         color: kPrimaryColor,
      //         strokeWidth: 5,
      //       ),
      //     );
      //   }
      //else { return


      Column(
        children: [
          const SizedBox(
            height: 40,
          ),
          Expanded(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  color: Colors.white,
                  child:

                  PageView.builder(itemBuilder: (context, index) {
                    return Padding(
                        padding: EdgeInsets.only(bottom: 1),
                        child: Image.file(
                       File(productmodel.imageUrl,),
                          fit: BoxFit.contain,
                        ));
                  }),
                ),
              )),
          Container(
            width: double.infinity,
            height: 400,
            decoration: const BoxDecoration(
              color: kPrimaryColor,
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(38),
                  topLeft: Radius.circular(38)),
            ),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(
                    top: 20,
                    left: 30,
                    right: 30,),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        '${productmodel.name}',

                        style: const TextStyle(
                          color: Colors.white,
                          fontSize: 25,
                          fontStyle: FontStyle.italic,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                      Icon(
                        Icons.favorite_outline,
                        color: kPrimaryColor,
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 30,
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    top: 10,
                    left: 20,
                    right: 20,
                  ),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          child: Center(
                            child: Text(
                              '${productmodel.price}'
                              ,
                              style: const TextStyle(
                                color: Colors.white,
                                fontSize: 15,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          height: 40,
                          width: 120,
                          decoration:
                          BoxDecoration(
                            borderRadius: const BorderRadius.all(
                                Radius.circular(15)),
                            color: kSelectedColor,
                          ),
                        ),
                        Container(
                          child:  Center(
                            child: Text(

                              '${productmodel.expDate}'
                              //'${controller.detailsproduct['exp_date']}
                              ,
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 10,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          height: 40,
                          width: 120,
                          decoration:
                          BoxDecoration(
                            borderRadius: const BorderRadius.all(
                                Radius.circular(15)),
                            color: kSelectedColor,
                          ),
                        ),
                      ]),
                ),
                const SizedBox(height: 40),
                const Padding(padding: EdgeInsets.all(10),
                  child: Text(
                    'here we write the description of the product ',
                    style: TextStyle(
                      fontSize: 20,
                      color: Colors.white,
                    ),
                  ),),

                const SizedBox(height: 117,),
                Container(
                  width: double.infinity,
                  decoration: BoxDecoration(
                      color: kSelectedColor,
                      borderRadius: const BorderRadius.only(
                          topRight: Radius.circular(15),
                          topLeft: Radius.circular(10))
                  ),
                  child: MaterialButton(
                    height: 40, onPressed: () {
                    //  controllerAdd.funProdContr(controller.detailsproduct);

                  },
                    child: const Text(
                      'Add to Cart',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 25,
                      ),
                    ),
                  ),
                ),
              ],),
          ),
        ],
      )
    ));

}}
