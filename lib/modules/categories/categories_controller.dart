
import 'package:get/get.dart';
import 'package:shop1/config/userinfo.dart';
import 'package:shop1/models/categories_model.dart';
import 'package:shop1/modules/products/products_controller.dart';
import 'categories_service.dart';

class   CategoriesController extends GetxController{

  List categoriesList = <Category>[].obs;
  List catproductlist = <Category>[].obs;
  CategoriesService _service = CategoriesService();
  ProductsController productsController=ProductsController();
  var isLoading  = true.obs;
  var currentIndex = 1.obs;

  CategoriesController() {
    loadCategories();
  }
  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() async {

   await changeCategories(currentIndex.value);
   // isLoading(false);
    super.onReady();
  }

  changeCategories(index) async {
    currentIndex(index);
    ///print(index);
    catproductlist=await productsController.loadProductsFromRepo(index);
  }


  loadCategories()async{
  categoriesList =  await _service.getCategories(Userinfo.USER_TOKEN);
}
}