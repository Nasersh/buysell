import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:shop1/config/server_config.dart';
import 'package:shop1/models/categories_model.dart';


class CategoriesService{

  var message;
  var url = Uri.parse(ServerConfig.domainNameServer + ServerConfig.getCategories);

  Future<List<Category>> getCategories(String token) async{
    var response = await http.get(url);
    print(response.statusCode);
   // print(response.body);

    if(response.statusCode == 200){
      var categories = categoriesFromJson(response.body);
      return categories.categories;
    }
    else {
      return [];
    }
  }
}