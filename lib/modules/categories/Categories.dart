import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shop1/modules/products/products_controller.dart';
import 'package:shop1/shared/Product_card.dart';

import '../../constants.dart';
import 'categories_controller.dart';

class Categories extends StatelessWidget {
  Categories({Key? key}) : super(key: key);

  CategoriesController catcontroller = Get.put(CategoriesController());
    ProductsController controller = Get.put(ProductsController());


  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Column(
        children: [
        Container(
          child: Column(
            children: [
              const SizedBox(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'Categories',
                    style: GoogleFonts.oswald(
                      textStyle: const TextStyle(
                          color: Colors.white,
                          fontSize: 25,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Container(
                    height: 80,
                    width: 80,
                    child: const Image(
                      image: AssetImage('images/basket.png'),
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 15,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 30),
                child: _buildCategoriesRow(),
              ),
              const SizedBox(height: 10,),
            ],
          ),
        height: 180,
        decoration: const BoxDecoration(
            color: kPrimaryColor,
            borderRadius: BorderRadius.only(bottomLeft: Radius.circular(40),bottomRight: Radius.circular(40)),

        ),),
          Expanded(
            child: Stack(children: [
              Container(
                // margin: EdgeInsets.only(top: 0),
                decoration: const BoxDecoration(
                  color: kBackgroundColor,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(40),
                    topRight: Radius.circular(40),
                  ),
                ),
              ),
              Obx(() {
                if (controller.isloading.value) {
                  return const Center(
                    child: CircularProgressIndicator(
                      color: kPrimaryColor,
                      strokeWidth: 5,
                    ),
                  );
                } else {
                  return ListView(primary: false, children: [
                    const SizedBox(
                      height: 30,
                    ),
                    GridView.count(
                      physics: ClampingScrollPhysics(),
                      padding: EdgeInsets.fromLTRB(8, 0, 8, 0),
                      shrinkWrap: true,
                      crossAxisCount: 2,
                      mainAxisSpacing: 30,
                      crossAxisSpacing: 20,
                      children:
                      List.generate(catcontroller.catproductlist.length, (index) {
                        return Buildcard(
                            catcontroller.catproductlist[index].name,
                            catcontroller.catproductlist[index].id,
                            catcontroller.catproductlist[index].imageUrl,
                            false, catcontroller.catproductlist[index]);
                      }),
                    )
                  ]);
                }
              }),
            ]),
          ),

        ],
      ),
    );
  }

  Container _buildCategoriesRow() {
    return Container(
      height: 35.0,
      margin: EdgeInsets.only(top: 16),
      child: ListView.builder(
          itemCount: catcontroller.categoriesList.length,
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index) => Obx(
                () => InkWell(
              onTap: () {
                catcontroller.changeCategories(catcontroller.categoriesList[index].id);
              },
              child: Container(
                margin: EdgeInsets.only(right: 8),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(4.0),
                  color: catcontroller.categoriesList[index].id == catcontroller.currentIndex.value
                      ? Colors.white.withOpacity(0.4)
                      : Colors.transparent,
                ),
                padding: const EdgeInsets.symmetric(
                  horizontal: 25.0,
                  vertical: 8.0,
                ),
                child: Text(
                  catcontroller.categoriesList[index].name,
                  style: const TextStyle(
                      fontSize: 15,
                      color: Colors.white

                  ),
                ),
              ),
            ),
          )),
    );
  }
}
