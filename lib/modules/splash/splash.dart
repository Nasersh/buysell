
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../constants.dart';
import 'splash_conroller.dart';

class Splash extends StatelessWidget {
  SplashController controller = Get.put(SplashController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          decoration: gradientBackground,
          child: Image.asset('images/basket.png'),
        ),
      ),
    );
  }
}
