
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:shop1/config/userinfo.dart';
import 'package:shop1/modules/splash/splash_service.dart';
import 'package:shop1/native_service/secure_storage.dart';

class SplashController extends GetxController{

  late SecureStorage _storage;
  late bool validityToken;
  late SplashService _service;

  @override
  void onInit() async {
    validityToken = false;
    _service = SplashService();
    _storage = SecureStorage();
    await checkToken();
    super.onInit();
  }



  Future<void> checkToken() async{
    String? token = await _storage.read('token');
    if(token != null){
     await validToken(token);
      if(validityToken){
        Userinfo.USER_TOKEN = token;
        Get.offAllNamed('/home');
      }
      else{
        EasyLoading.showError(_service.message);
        Get.offAllNamed('/login');
      }
    }
    else{
      Get.offNamed('/login');
    }
  }

  Future<void> validToken(String token) async{
    validityToken = await _service.checkValid(token);
  }


}