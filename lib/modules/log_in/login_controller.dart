import 'package:get/get.dart';
import 'package:shop1/models/user.dart';
import 'package:shop1/modules/in_app/app.dart';
import 'package:shop1/modules/log_in/login_service.dart';
import 'package:shop1/modules/profile/profile_controller.dart';
import 'package:shop1/native_service/secure_storage.dart';

class LoginController extends GetxController
{

  late String email ;
  var password ;
  var name ;
  var loginStatus ;
  var message;
  var checkBoxStatus;
  var out=false.obs;
  late LoginService service ;
  late SecureStorage storage;
  @override
  void onInit()
  async{

    storage = SecureStorage();
   CheckToken();
    email ='';
    name ='';
  password='';
  loginStatus=false;
  message='';
  checkBoxStatus=false;
  service= LoginService();
  super.onInit();
  }

  Future<void> SigninOnClick() async{

    UserClass user=UserClass(email: email,password: password,);
    loginStatus =await service.Signin(user);
    message=service.message;
  }

    Future<void>CheckToken()async{
   var token=await storage.read('token');
    if(token=='')
    {

    }
    else
    {
      Get.off(InApp());
    }
  }

}