import 'dart:convert';
import 'package:get/get.dart';
import 'package:shop1/config/server_config.dart';
import 'package:http/http.dart' as http;
import 'package:shop1/config/userinfo.dart';
import 'package:shop1/models/user.dart';
import 'package:shop1/modules/in_app/app.dart';
import 'package:shop1/native_service/secure_storage.dart';



class LoginService
{
  var message;
  var token;
  var userid;
  var name;

  var url = Uri.parse(ServerConfig.domainNameServer + ServerConfig.Signin);

  Future <bool> Signin( UserClass user ) async{
    var response = await http.post(
        url,headers: {
      'Accept' : 'application/json',
    },
        body: {
          'email': user.email,
          'password':user.password,
        }
    );
    print(response.statusCode);
    if(response.statusCode==200)
    {
     var jsonresponse=jsonDecode(response.body);
      userid=jsonresponse['user_id'];
     print('id=$userid');
     var userobj=User.fromJson(jsonresponse);
     user=userobj.user;
     name=user.name;
     print(name);
     message=jsonresponse['message'];
     token=jsonresponse['token'];
     Userinfo.USER_TOKEN=token;
     Userinfo.USER_NAME=name;
     Userinfo.USER_ID=userid.toString();
      SecureStorage storage=SecureStorage();
    await storage.save('token', Userinfo.USER_TOKEN);
    await storage.save('user_id', Userinfo.USER_ID);
    await storage.save('name', Userinfo.USER_NAME);
     Get.off(InApp());
      return true;
    }
    else if (response.statusCode == 401) {
      var jsonResponse = jsonDecode(response.body);
      message = jsonResponse['error'];
      return false;
    }else if (response.statusCode == 422) {
      var jsonResponse = jsonDecode(response.body);
      message = jsonResponse['error'];
      return false;
    }
    else if (response.statusCode == 500) {
      print('wrong');
      return false;
    }
    else {
      message = 'server error';
      return false;

} }
}