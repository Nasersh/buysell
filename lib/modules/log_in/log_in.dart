import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shop1/constants.dart';
import 'package:shop1/modules/in_app/app.dart';
import 'package:shop1/modules/log_in/login_controller.dart';
import 'package:shop1/modules/register/register.dart';
import 'package:shop1/shared/component.dart';

class LogIn extends StatelessWidget {

   LogIn({Key? key}) : super(key: key);
   LoginController controller=Get.put(LoginController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kBackgroundColor,
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(
          children: [
            Container(
              height: 350,
              width: double.infinity,
              child: Column(
                children: [
                  const SizedBox(height: 60,),
                  Text(
                    'Welcome To our Shop',
                    style: GoogleFonts.pacifico(
                        textStyle:
                        const TextStyle(color: Colors.white, fontSize: 30)),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  const Image(
                    height: 200,
                    width: 200,
                    image: AssetImage('images/robot.png'),
                  )
                ],
              ),
              decoration: const BoxDecoration(
                color: kPrimaryColor,
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(50),
                  bottomRight: Radius.circular(50),
                ),
              ),
            ),
            const SizedBox(
              height: 50,
            ),
            SignInText(htext: 'Enter your Email', ic: const Icon(
              Icons.person,color: kPrimaryColor,
            ),
                onchange: (value){
                controller.email=value;
                }),
            const SizedBox(
              height: 20,
            ),
            SignInText(htext: 'Password', ic: const Icon(
              Icons.lock,color: kPrimaryColor,
            ),
                onchange: (value){
              controller.password=value;
                }),
            const SizedBox(
              height: 40,
            ),
            ElevatedButton(
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(kPrimaryColor),
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(18.0),
                    side: const BorderSide(
                      color: kPrimaryColor,
                      width: 4.0,
                    ),
                  ),
                ),
              ),
              child: Text('Submit'),
              onPressed: () {
                ClickSignin();
              },

            ),
            const SizedBox(height: 20,),
            TextButton(onPressed: (){
              Get.to(Register());
            },
                child: const Text(
                    'Sign up!'
                    ,style:TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                    color: kPrimaryColor
                )
                ))
          ],
        ),
      ),
    );
  }

    void ClickSignin() async {
  EasyLoading.show(status: 'Loading ...');
   await  controller.SigninOnClick();
   if(controller.loginStatus)
   {
  EasyLoading.showSuccess(controller.message);
  Get.off(InApp());
   }
   else 
     {
       EasyLoading.showError(controller.message,duration: Duration(seconds: 2),dismissOnTap: true);
       print('Error');
     }
   }
}
