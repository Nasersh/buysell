import 'package:get/get.dart';
import 'package:shop1/config/server_config.dart';
import 'package:shop1/models/product.dart';
import 'myproductservice.dart';

class MyProductsController extends GetxController
{
  var productlist = <Product>[].obs;

  var id=0.obs;
  var showGrid = false.obs;

  MyProductService _service= MyProductService();
var isloading=true.obs;
var issloading=true.obs;
var loading=true.obs;

@override
void onInit() {
  fetchMyProducts();
  super.onInit();
}


  void fetchMyProducts() async {
  try {
    isloading(true);
    var products = await _service.getProducts();
    if (products != null) {
      productlist.value = products;
    }
  } finally {
    isloading(false);
    }
  }


}