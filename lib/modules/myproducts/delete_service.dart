
import 'dart:convert';
import 'package:shop1/config/server_config.dart';
import 'package:http/http.dart' as http;
import 'package:shop1/config/userinfo.dart';
import 'package:shop1/native_service/secure_storage.dart';

class DeleteService
{
  Delete(index) async {
    var urlshowproduct=Uri.parse(ServerConfig.domainNameServer+ServerConfig.deleteproduct+'${index}');

    var storage = SecureStorage();

    String? token = await storage.read('token');
print(token);
    var response = await http.delete(urlshowproduct,
        headers: {
          'Accept':'application/json',
          'Authorization' : 'Bearer $token'
        } );
    print(response.statusCode);
    print(response.body);

    if(response.statusCode == 200){
      print('done');
    }
  }



}