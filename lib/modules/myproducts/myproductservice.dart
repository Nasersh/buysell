

import 'dart:convert';
import 'package:shop1/config/server_config.dart';
import 'package:http/http.dart' as http;
import 'package:shop1/config/userinfo.dart';
import 'package:shop1/models/categories_model.dart';
import 'package:shop1/models/product.dart';


class MyProductService
{
  var message;
  var url = Uri.parse(ServerConfig.domainNameServer + ServerConfig.showUserProducts);

  Future<List<Product>> getProducts() async{
    print(Userinfo.USER_TOKEN);
    var response = await http.get(url,
        headers: {
          'Accept': 'application/json',
          'Authorization' : 'Bearer '+Userinfo.USER_TOKEN
        },
    );
    print(response.statusCode);

    if(response.statusCode == 200){
     var products = productsFromJson(response.body);
     return products.products;
    }
    else {
      return [];
    }
  }
}