import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shop1/config/server_config.dart';
import 'package:shop1/modules/in_product/in_product.dart';
import 'package:shop1/modules/in_product/inprodutcontroller.dart';
import 'package:shop1/modules/searchproducts/seacrh.dart';
import 'package:shop1/shared/Product_card.dart';
import 'package:shop1/shared/myProduct_card.dart';
import '../../constants.dart';
import 'deletecontroller.dart';
import 'myproducts_controller.dart';

class MyProd extends StatelessWidget {
  MyProd({Key? key}) : super(key: key);
  MyProductsController controller = Get.put(MyProductsController());
  InProductController incontroller = Get.put(InProductController());
  DeleteController dcontroller=Get.put(DeleteController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: kBackgroundColor,
        appBar: AppBar(
          backgroundColor: kPrimaryColor,
          elevation: 0,
          centerTitle: false,
          title: const Text('Nshop'),
          actions: <Widget>[
            IconButton(
              icon: const Icon(Icons.sort),
              onPressed: () {},
            ),
          ],
        ),
        body: SafeArea(
          bottom: false,
          child: Column(
            children: [
              Container(
                width: double.infinity,
                height: 180,
                decoration: const BoxDecoration(
                    color: kPrimaryColor,
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(40),
                        bottomRight: Radius.circular(40))),
                child: Column(
                  children: [
                    const SizedBox(
                      height: 45,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 35),
                      child: Text(
                        'My Products',
                        style: GoogleFonts.oswald(
                          textStyle: const TextStyle(
                              color: Colors.white,
                              fontSize: 25,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                  ],
                ),
              ),
              Expanded(
                child: Stack(children: [
                  Container(
                    // margin: EdgeInsets.only(top: 0),
                    decoration: const BoxDecoration(
                      color: kBackgroundColor,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(40),
                        topRight: Radius.circular(40),
                      ),
                    ),
                  ),
                  Obx(() {
                    if (controller.isloading.value) {
                      return const Center(
                        child: CircularProgressIndicator(
                          color: kPrimaryColor,
                          strokeWidth: 5,
                        ),
                      );
                    } else {
                      return ListView(primary: false, children: [
                        const SizedBox(
                          height: 30,
                        ),
                        GridView.count(
                          physics: ClampingScrollPhysics(),
                          padding: EdgeInsets.fromLTRB(8, 0, 8, 0),
                          shrinkWrap: true,
                          crossAxisCount: 2,
                          mainAxisSpacing: 30,
                          crossAxisSpacing: 20,
                          children: List.generate(controller.productlist.length,
                              (index) {
                            return BuildMycard(
                                controller.productlist[index].name,
                                controller.productlist[index].id,
                                controller.productlist[index].imageUrl,
                                false,
                                controller.productlist[index],
                                (){dcontroller.deleteData(controller.productlist[index].id);}
                            );
                          }),
                        )
                      ]);
                    }
                  }),
                ]),
              ),
            ],
          ),
        ));
  }

  ListView _buildProductsList() {
    return ListView.builder(
      itemCount: controller.productlist.length,
      padding: EdgeInsets.only(top: 16),
      itemBuilder: (context, index) => Card(
        elevation: 0.0,
        child: Container(
          height: 120,
          padding: EdgeInsets.all(16),
          margin: const EdgeInsets.only(bottom: 8.0),
          child: Row(
            children: [
              Container(
                width: 100,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: NetworkImage(
                        "\$${controller.productlist[index].imageUrl}"),
                    fit: BoxFit.fill,
                  ),
                ),
              ),
              SizedBox(width: 8),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "\$${controller.productlist[index].name}",
                        style: const TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                      Expanded(
                        child: Text(
                          "\$${controller.productlist[index].price}",
                          maxLines: 3,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                      Text(
                        "\$${controller.productlist[index].price}",
                        style: const TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
