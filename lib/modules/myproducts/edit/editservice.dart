

import 'package:shop1/config/server_config.dart';
import 'package:shop1/models/product.dart';
import 'package:shop1/native_service/secure_storage.dart';
import 'package:http/http.dart' as http;

funUpdate(Product product)async
{
  var urlUpdate=Uri.parse(ServerConfig.domainNameServer+ ServerConfig.updateProduct+'${product.id}');

  var _storage = SecureStorage();
  String? token = await _storage.read('token');

  var respon=await http.post(urlUpdate, headers:{
    'Accept' : 'application/json',
    'Authorization' : 'Bearer $token'   }

    ,body: {

      'date':product.expDate,
      'name':product.name,
      'price' :product.price,
      'categoryId':product.categoryId,
      'quantity'  :product.quantity,
    }, );

  print(respon.statusCode);
  if(respon.statusCode==200)
  {
    return product;
  }
  else
  {
    print('error');
    return ;
  }





}
