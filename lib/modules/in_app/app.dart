import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:get/get_navigation/src/extension_navigation.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shop1/config/userinfo.dart';
import 'package:shop1/modules/additions/add_new.dart';
import 'package:shop1/modules/log_in/log_in.dart';
import 'package:shop1/modules/myproducts/myprod.dart';
import 'package:shop1/modules/products/prod.dart';
import 'package:shop1/modules/profile/profile.dart';
import 'package:shop1/modules/profile/profile_controller.dart';
import 'package:shop1/native_service/secure_storage.dart';
import '../../constants.dart';
import 'package:get/get.dart';
import '../categories/Categories.dart';
import 'inapp_controller.dart';
import 'logout controller.dart';

class InApp extends StatelessWidget {
   InApp({Key? key}) : super(key: key);
  int x = 0;
   ProfileController pcontroller = Get.put(ProfileController());

  InAppController incontroller=Get.put(InAppController());
  List<Widget> screen = [Prod() ,Categories()];
  LogoutController logcontroller=Get.put(LogoutController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor:kBackgroundColor,
      appBar: AppBar(
        backgroundColor: kPrimaryColor,
        elevation: 0,
        centerTitle: false,
        title: const Text('Nshop'),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.sort),
            onPressed: () {},
          ),
        ],
      ),
      drawer: Drawer(
        elevation: 20,
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            DrawerHeader(
              decoration: const BoxDecoration(
                  color:kPrimaryColor,
                  borderRadius: BorderRadius.only(bottomRight: Radius.circular(20))
                // borderRadius: BorderRadius.only(bottomRight: Radius.circular(60))
              ),
              child: Center(

                child: Obx(()=>
                   Text(
                    'Hello ${pcontroller.name.value}',
                  //  'Hello ${incontroller.name.value}',
                      style:GoogleFonts.oswald(
                          textStyle: const TextStyle(
                            fontSize: 40,
                            color: Colors.white,
                          )
                      )
                  ),
                ),
              ),
            ),
            ListTile(
              leading: const Icon(
                Icons.person_outline,
                color: kPrimaryColor,
              ),
              title: const Text('Personal Account'),
              onTap: () {
                print(Userinfo.USER_ID);
                Get.to(() => Profile());
              },
            ),
            const Divider(
              height: 10,
              thickness: 0.5,
              color: Colors.grey,
            ),
            ListTile(
              leading: const Icon(
                Icons.star_border_outlined,
                color: kPrimaryColor,
              ),
              title: const Text('Theme'),
              onTap: () {
                // Update the state of the app.
                // ...
              },
            ),
            const Divider(
              height: 5,
              thickness: 0.5,
              color: Colors.grey,
            ),
            ListTile(
              leading: const Icon(
                Icons.shopping_cart,
                color: kPrimaryColor,
              ),
              title: const Text('my product'),
              onTap: () {
                Get.to(()=>MyProd());
                // ...
              },
            ),
            const Divider(
              height: 5,
              thickness: 0.5,
              color: Colors.grey,
            ),
            ListTile(
              leading: const Icon(
                Icons.logout,
                color: kPrimaryColor,
              ),
              title: const Text('Log Out'),
              onTap: () async{
                var storage = SecureStorage();
                logcontroller.LogoutData();
                storage.save('token', '');
                Get.to(LogIn());
              },
            ),


          ],
        ),
      ),
      bottomNavigationBar:BottomAppBar(
          shape: const CircularNotchedRectangle(),
          notchMargin: 9.0,
          color: kBackgroundColor,
          elevation: 10.0,
          clipBehavior: Clip.antiAlias,
          child: Container(
            height: 50,
            decoration: const BoxDecoration(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(25),
                topRight: Radius.circular(25),
              ),
              color: Colors.white,
            ),
            child:
            Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    height: 50,
                    width: MediaQuery.of(context).size.width / 2 - 40,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children:  [
                        IconButton(
                          color: kPrimaryColor, icon: const Icon(
                          Icons.home,
                        ), onPressed: () {
                          incontroller.fun(0);
                        },
                        ),

                      ],
                    ),
                  ),
                  Container(
                    height: 50,
                    width: MediaQuery.of(context).size.width / 2 - 40,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children:  [

                        IconButton(
                          color: kPrimaryColor, icon: const Icon( Icons.shopping_basket,
                        ), onPressed: () {
                          incontroller.fun(1);
                        },
                        ),
                      ],
                    ),
                  ),
                ]),
          )),
      floatingActionButtonLocation:FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingActionButton(
        backgroundColor: kPrimaryColor, onPressed: () { Get.to( AddNew());},
        child: const Icon(
            Icons.add
        ),
      ),
      body: Obx((){
        if(incontroller.currentIndex==0)
          return Prod();

        else
        {
          return Categories();

        }  }
      ),
    );
  }
}

