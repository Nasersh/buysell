import 'package:get/get.dart';
import 'package:shop1/models/user.dart';
import 'package:shop1/modules/profile/profile_controller.dart';
import 'package:shop1/native_service/secure_storage.dart';

class InAppController extends GetxController
{

  SecureStorage storage=new SecureStorage();
  var currentIndex=0.obs;
  var name=''.obs;
  fun(index)
  {
    currentIndex(index);
  }
  @override
  void onInit() {
    // ProfileController pc=ProfileController();
    super.onInit();
  }
  UserProfile() async {
   var un =await storage.read('name');
    name.value=un!;
    print(name.value);
  }

}