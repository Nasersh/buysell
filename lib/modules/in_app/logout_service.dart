
import 'package:shop1/config/server_config.dart';
import 'package:shop1/native_service/secure_storage.dart';
import 'package:http/http.dart' as http;


class LogoutService
{
  Logout() async {
    var urlshowproduct=Uri.parse(ServerConfig.domainNameServer+ServerConfig.Logout);

    var storage = SecureStorage();

    String? token = await storage.read('token');
    print(token);
    var response = await http.post(urlshowproduct,
        headers: {
          'Accept':'application/json',
          'Authorization' : 'Bearer $token'
        } );
    print(response.statusCode);
   // print(response.body);

    if(response.statusCode == 200){
      print('done');
    }

  }



}