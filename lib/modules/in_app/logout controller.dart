
import 'package:get/get.dart';

import 'logout_service.dart';

class LogoutController extends GetxController{

  void LogoutData() async {

    LogoutService _service=LogoutService();
   await _service.Logout();
  }
}