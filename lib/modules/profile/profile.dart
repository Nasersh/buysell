import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shop1/modules/profile/profile_controller.dart';
import 'package:shop1/shared/myProduct_card.dart';
import '../../constants.dart';
import 'package:url_launcher/url_launcher.dart';
class Profile extends StatelessWidget {
  Profile({Key? key}) : super(key: key);
  ProfileController controller = Get.put(ProfileController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: kBackgroundColor,
        appBar: AppBar(
          backgroundColor: kPrimaryColor,
          elevation: 0,
          centerTitle: false,
          title: const Text('Nshop'),

        ),
        body: SafeArea(
          bottom: false,
          child: Column(
            children: [
              Container(
                width: double.infinity,
                height: 180,
                decoration: const BoxDecoration(
                    color: kPrimaryColor,
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(40),
                        bottomRight: Radius.circular(40))),
                child: Column(
                  children: [
                    const SizedBox(
                      height: 30,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 35),
                      child:
                      Column(
                        children:  [
                          CircleAvatar(
                            radius: 70,
                             // backgroundImage: NetworkImage(controller.image),
                              backgroundColor: kBackgroundColor,
                          )

                        ],
                      )
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                  ],
                ),
              ),
              Expanded(
                child: Stack(children: [
                  Container(
                    decoration: const BoxDecoration(
                      color: kBackgroundColor,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(40),
                        topRight: Radius.circular(40),
                      ),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 40,vertical: 40),
                      child: Obx(()=>
                         Column(
                          children: [
                            Text(
                              'Profile',
                              style: GoogleFonts.oswald(
                                textStyle: const TextStyle(
                                    color: kPrimaryColor,
                                    fontSize: 25,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                            const SizedBox(
                              height: 50,
                            ),
                            Row(
                             // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children:  [
                                const Text(
                                  'Name:',style: TextStyle(
                                    color: kPrimaryColor,
                                    fontSize: 17,
                                  fontWeight: FontWeight.bold
                                ),
                                ),
                                const SizedBox(width: 40,),
                                Text(
                                controller.name.value,style: const TextStyle(
                                    fontSize: 17
                                ),
                                ),
                              ],
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Divider(),
                            const SizedBox(
                              height: 10,
                            ),
                            Row(
                             // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children:  [
                                const Text(
                                  'Phone:',style: TextStyle(
                                    color: kPrimaryColor,
                                    fontSize: 17 ,fontWeight: FontWeight.bold
                                ),
                                ),
                                SizedBox(width: 20,),
                                FlatButton(
                                  onPressed: () { _launchPhoneURL('0940749492'); },
                                  child:  Text(
                                    controller.phone.value,style: TextStyle(
                                      fontSize: 17
                                  ),
                                  ),
                                ),
                              ],
                            ),
                            Divider(),
                            const SizedBox(
                              height: 30,
                            ),
                            Row(
                             // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children:   [
                                Text(
                                  'Email:',style: TextStyle(
                                  color: kPrimaryColor,
                                  fontSize: 17,fontWeight: FontWeight.bold
                                ),
                                ),
                                SizedBox(width: 40,),
                                Text(
                                controller.email.value,style: TextStyle(

                                    fontSize: 17
                                ),
                                ),
                              ],
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Divider(),
                          ],
                        ),
                      ),
                    ),
                  ),

                ]),
              ),
            ],
          ),
        ));
  }
  _launchPhoneURL(String phoneNumber) async {
    String url = 'tel://$phoneNumber';
    launch(url);
  }
}
