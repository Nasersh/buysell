import 'package:get/get.dart';
import 'package:shop1/config/userinfo.dart';
import 'package:shop1/models/user.dart';
import 'package:shop1/modules/profile/profile_service.dart';
import 'package:shop1/native_service/secure_storage.dart';

class ProfileController extends GetxController
{

  ProfileService service=ProfileService();
  SecureStorage storage=new SecureStorage();
  var name=''.obs;
  var phone=''.obs;
  var email=''.obs;
  var image=''.obs;
  var facebook=''.obs;
  @override
  void onInit() {
    UserProfile();
    super.onInit();
  }
  @override
  void onReady()async
  {

    super.onReady();
  }
  UserProfile() async {
    var id=await storage.read('user_id');
    print(id);
    UserClass user=await service.fetchProfileDetails(id);

    phone.value= user.phone;
    email.value= user.email;
    name.value= user.name;
    image.value= user.profileImgUrl;
    facebook.value= user.facebookUrl;

  }
  @override
  void onClose() {
name.value='';
super.onClose();
  }

}