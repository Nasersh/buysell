import 'dart:convert';

import 'package:shop1/config/server_config.dart';
import 'package:http/http.dart' as http;
import 'package:shop1/models/user.dart';

class ProfileService
{
  fetchProfileDetails(index) async {
    var urlshowprofile=Uri.parse(ServerConfig.domainNameServer+ServerConfig.showUserDetails+'$index');

    var response = await http.get(urlshowprofile);
    print(response.statusCode);
   // print(response.body);

    if(response.statusCode == 200){

     User user=userFromJson(response.body);

      return user.user;

    }
    else{
      return {};
    }
  }



}