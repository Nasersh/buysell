import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shop1/config/server_config.dart';
import 'package:shop1/modules/in_product/in_product.dart';
import 'package:shop1/modules/in_product/inprodutcontroller.dart';
import 'package:shop1/modules/searchproducts/seacrh.dart';
import 'package:shop1/shared/Product_card.dart';
import '../../constants.dart';
import 'products_controller.dart';

class Prod extends StatelessWidget {
  Prod({Key? key}) : super(key: key);
  ProductsController controller = Get.put(ProductsController());
  InProductController incontroller=Get.put(InProductController());

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      bottom: false,
      child: Column(
        children: [
          Container(
            height: 180,
            decoration: const BoxDecoration(
                color: kPrimaryColor,
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(40),
                    bottomRight: Radius.circular(40))),
            child: Column(
              children: [
                Container(
                    margin: EdgeInsets.all(kDefaultPadding),
                    padding: const EdgeInsets.symmetric(
                      horizontal: kDefaultPadding,
                      vertical: kDefaultPadding / 4,
                    ),
                    decoration: BoxDecoration(
                      color: Colors.white.withOpacity(0.4),
                      borderRadius: BorderRadius.circular(12),
                    ),
                    child: FlatButton(
                        onPressed: () {Get.to(ssearch());},
                        child: Row(
                          children: [
                            Icon(
                              Icons.search,
                              color: kSelectedColor,
                            ),
                            const SizedBox(
                              width: 20,
                            ),
                            Text(
                              'Search',
                              style: TextStyle(
                                  color: kSelectedColor, fontSize: 20),
                            ),
                          ],
                        ))),
                const SizedBox(
                  height: 15,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 35),
                  child: Text(
                    'All Products',
                    style: GoogleFonts.oswald(
                      textStyle: const TextStyle(
                          color: Colors.white,
                          fontSize: 25,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
              ],
            ),
          ),
          Expanded(
            child: Stack(children: [
              Container(
                // margin: EdgeInsets.only(top: 0),
                decoration: const BoxDecoration(
                  color: kBackgroundColor,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(40),
                    topRight: Radius.circular(40),
                  ),
                ),
              ),
              Obx(() {
                if (controller.isloading.value) {
                  return const Center(
                    child: CircularProgressIndicator(
                      color: kPrimaryColor,
                      strokeWidth: 5,
                    ),
                  );
                } else {
                  return ListView(primary: false, children: [
                    const SizedBox(
                      height: 30,
                    ),
                    GridView.count(
                      physics: ClampingScrollPhysics(),
                      padding: EdgeInsets.fromLTRB(8, 0, 8, 0),
                      shrinkWrap: true,
                      crossAxisCount: 2,
                      mainAxisSpacing: 30,
                      crossAxisSpacing: 20,
                      children:
                          List.generate(controller.productlist.length, (index) {
                        return Buildcard(
                            controller.productlist[index].name,
                            controller.productlist[index].id,
                            controller.productlist[index].imageUrl,
                            false,controller.productlist[index]);
                      }),
                    )
                  ]);
                }
              }),
            ]),
          ),
        ],
      ),
    );
  }

  ListView _buildProductsList() {
    return ListView.builder(
      itemCount: controller.productlist.length,
      padding: EdgeInsets.only(top: 16),
      itemBuilder: (context, index) => Card(
        elevation: 0.0,
        child: Container(
          height: 120,
          padding: EdgeInsets.all(16),
          margin: const EdgeInsets.only(bottom: 8.0),
          child: Row(
            children: [
              Container(
                width: 100,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: NetworkImage(
                        "${controller.productlist[index].imageUrl}"),
                    fit: BoxFit.fill,
                  ),
                ),
              ),
              SizedBox(width: 8),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "${controller.productlist[index].name}",
                        style: const TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                      Expanded(
                        child: Text(
                          "\$${controller.productlist[index].price}",
                          maxLines: 3,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                      Text(
                        "\$${controller.productlist[index].price}",
                        style: const TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
