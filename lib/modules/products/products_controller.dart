  import 'package:get/get.dart';
  import 'package:shop1/config/server_config.dart';
  import 'package:shop1/models/product.dart';
  import 'productservice.dart';

  class ProductsController extends GetxController
  {
    var productlist = <Product>[].obs;
    var catproductlist = <Product>[].obs;
    var searchproductlist = <Product>[].obs;

    var id=0.obs;
    var showGrid = false.obs;

    ProductService _service= ProductService();
  var isloading=true.obs;
  var issloading=true.obs;
  var loading=true.obs;

  @override
  void onInit() {
    fetchProducts();
   //loadProductsFromRepo(id.value);
    super.onInit();
  }

    loadProductsFromRepo(int index) async {
      loading(true);
      id.value=index;
        catproductlist.value = await _service.loadProductsFromApi(index);
      loading(false);
        return catproductlist;

    }
  
    void fetchProducts() async {
    try {
      isloading(true);
      var products = await _service.getProducts();
      if (products != null) {
        productlist.value = products;
      }
    } finally {
      isloading(false);
      }
    }

    searchProductsFromRepo(String name) async {
      issloading(true);
      searchproductlist.value = await _service.SearchProducts(name);
      issloading(false);
      return searchproductlist;

    }



  }