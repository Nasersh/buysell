
import 'package:shop1/config/server_config.dart';
import 'package:http/http.dart' as http;
import 'package:shop1/models/product.dart';


class ProductService
{
  var message;
  var url = Uri.parse(ServerConfig.domainNameServer + ServerConfig.showallProducts);

  Future<List<Product>> getProducts() async{
    var response = await http.get(url);
    print(response.statusCode);

    if(response.statusCode == 200){
     var products = productsFromJson(response.body);
     return products.products;
    }
    else {
      return [];
    }
  }
  Future<List<Product>> loadProductsFromApi(int index) async {
    var response = await http.get(Uri.parse(ServerConfig.domainNameServer+ServerConfig.showCategoryProducts + '${index}'),
        headers: {
          'Accept': 'application/json',
        },
    );
    print(response.statusCode);
  //  print(response.body);

    if(response.statusCode == 200){
      var porductscat = productsFromJson(response.body);
      return porductscat.products;
    }
    else {
      return [];
    }
  }


  Future<List<Product>> SearchProducts(String name) async {
    var response = await http.get(Uri.parse(ServerConfig.domainNameServer+ServerConfig.SearchProducts + '${name}'),
      headers: {
        'Accept': 'application/json',
      },
    );
    print(response.statusCode);
  //  print(response.body);

    if(response.statusCode == 200){
      var porductsearch = productsFromJson(response.body);
      return porductsearch.products;
    }
    else {
      return [];
    }
  }

}