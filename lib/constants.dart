import 'package:flutter/material.dart';

const Color firstBackgroundColor = Color(0xff051365);
const Color secondBackgroundColor = Color(0xff172CA1);
const Color thirdBackgroundColor = Color(0xff556AEF);
const Color dividerColor = Color(0xff485CDF);
const Color black = Color(0xff000000);
const Color white = Color(0xffffffff);
const Color mainFontColor = Color(0xff4E62E6);

const Color purpleBack = Color.fromRGBO(181, 151, 246, 1);
const Color blueBack = Color.fromRGBO(80, 165, 228, 1);

const Color purpleDark = Color.fromRGBO(68, 55, 245, 1);
const Color purpleLight = Color.fromRGBO(68, 55, 245, 0.1);

const Color circlePurpleLight = Color.fromRGBO(68, 55, 245, 0.70);
const Color circlePurpleDark = Color.fromRGBO(68, 55, 200, 1);


const BoxDecoration gradientBackground = BoxDecoration(
  gradient: LinearGradient(
    begin: Alignment(-1, -1),
    end: Alignment(1, 1),
    colors: [
      firstBackgroundColor,
      secondBackgroundColor,
      thirdBackgroundColor
    ],
  ),
);


const kBackgroundColor = Color(0xFFF1EFF1);
const kPrimaryColor = Color(0xFF035AA6);
const kSecondaryColor = Color(0xFFFFA41B);
const kTextColor = Color(0xFF000839);
const kTextLightColor = Color(0xFF747474);
const kBlueColor = Color(0xFF40BAD5);
var kSelectedColor =Colors.white.withOpacity(0.4);

const kDefaultPadding = 20.0;

// our default Shadow
const kDefaultShadow = BoxShadow(
  offset: Offset(0, 15),
  blurRadius: 27,
  color: Colors.black12, // Black color with 12% opacity
);

